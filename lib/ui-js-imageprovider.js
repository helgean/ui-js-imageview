
class UiJsImageprovider {

  constructor(url) {
    this.url = new URL(url);
    this.searchParams = Object.fromEntries(url.searchParams);
    this.url.search = '';
  }

  getUrl(file, page, width, height, contentType) {
    const searchParams = Object.assign(this.searchParams, {
      file: file,
      page: page,
      width: width,
      heigh: height,
      contentType: contentType
    });
    const search = Object
      .keys(searchParams)
      .reduce((acc, cur) => acc.concat(`${cur}=${encodeURIComponent(searchParams[cur])}`), '?');
    return `${this.url.href}${search}`;
  }

  async getImage(file, page, width, height, contentType) {
    const url = getUrl(file, page, width, height, contentType);
    const image = new Image();
    image.src = url;
    await image.decode();
    return image;
  }

}